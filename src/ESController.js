'use strict'

import autoBind from 'auto-bind'
import ResponseBody from './ResponseBody'

export default class ESController {
  constructor (Model) {
    this.Model = Model

    autoBind(this)
  }

  async createIndex (request, response, next) {
    const { Model } = this

    const esResponse = await Model.createIndex()
    response.body = new ResponseBody(200, 'Create Index Success', esResponse)

    next()
  }

  async removeIndex (request, response, next) {
    const { Model } = this

    const esResponse = await Model.removeIndex()
    response.body = new ResponseBody(200, 'Remove Index Success', esResponse)

    next()
  }

  async create (request, response, next) {
    const { Model } = this
    const { body: { data } } = request

    const esResponse = await Model.create(data)
    response.body = new ResponseBody(201, 'Create Success', esResponse)

    next()
  }

  async bulkCreate (request, response, next) {
    const { Model } = this
    const { body: { data } } = request

    const esResponse = await Model.bulkCreate(data)
    response.body = new ResponseBody(200, 'Bulk Create Success', esResponse)

    next()
  }

  async recreate (request, response, next) {
    const { Model } = this
    const { body: { data } } = request

    const esResponse = await Model.recreate(data)
    response.body = new ResponseBody(200, 'Recreate Success', esResponse)

    next()
  }

  async findById (request, response, next) {
    const { Model } = this
    const { params } = request
    const { id } = params

    const esResponse = await Model.findById(id)
    response.body = new ResponseBody(200, 'Find by Id Success', esResponse)

    next()
  }

  async search (request, response, next) {
    const { Model } = this
    const { query } = request
    const searchQuery = { match: query }
    const searchBody = { query: searchQuery }

    const esResponse = await Model.search(searchBody)
    response.body = new ResponseBody(200, 'Search Success', esResponse)

    next()
  }

  async list (request, response, next) {
    const { Model } = this

    const esResponse = await Model.list()
    response.body = new ResponseBody(200, 'List Success', esResponse)

    next()
  }

  async update (request, response, next) {
    const { Model } = this
    const { body: { data } } = request
    const { params } = request
    const { id } = params

    // TODO: Validate data.id and params.id
    data.id = id

    const esResponse = await Model.update(data)
    response.body = new ResponseBody(200, 'Update Success', esResponse)

    next()
  }

  async bulkUpdate (request, response, next) {
    const { Model } = this
    const { body: { data } } = request

    const esResponse = await Model.bulkUpdate(data)
    response.body = new ResponseBody(200, 'Bulk Update Success', esResponse)

    next()
  }

  async remove (request, response, next) {
    const { Model } = this
    const { params } = request
    const { id } = params

    await Model.remove(id)
    response.body = new ResponseBody(200, 'Remove Success')

    next()
  }

  async bulkRemove (request, response, next) {
    const { Model } = this
    const { body: { data } } = request

    const esResponse = await Model.bulkRemove(data)
    response.body = new ResponseBody(200, 'Bulk Remove Success', esResponse)

    next()
  }
}
