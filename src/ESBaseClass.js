'use strict'

import _ from 'lodash'
import uuid4 from 'uuid/v4'

export default class ESBaseClass {
  constructor (body = {}, options = {}) {
    const _this = this
    const { PROPERTIES = {} } = options

    Object.keys(PROPERTIES).forEach(prop => {
      const config = PROPERTIES[prop]
      const { type } = config
      let value = body[prop] || ''

      if (type === 'integer' || type === 'long') {
        value = _.isArray(value) ? value : Number(value)
      }

      if (type === 'boolean') {
        value = value === true || _.chain(value).toString().lowerCase().trim().value() === 'true'
      }

      if (type === 'text' || type === 'keyword') {
        value = _.isArray(value) ? value : _.chain(value).toString().value()
      }

      if (type === 'object') {
        value = _.isObject(value) ? value : {}
      }

      _this[prop] = value
    })

    if (options.timestamp === true) {
      this.createdAt = body.createdAt || new Date().getTime()
      this.updatedAt = body.updatedAt || this.createdAt
    }

    this.id = this.id || (options.generateId && uuid4()) || ''
  }
}
