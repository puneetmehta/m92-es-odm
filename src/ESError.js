'use strict'

import autoBind from 'auto-bind'
import ResponseBody from './ResponseBody'

const ERROR_NAME = 'ESError'
const ERROR_CLASSIFICATION = 'DATABASE_ERROR'

const CAN_CAPTURE = typeof Error.captureStackTrace === 'function'
const CAN_STACK = !!new Error().stack

export default class ESError extends Error {
  constructor (error) {
    const {
      _isESError,
      message,
      msg,
      name,
      statusCode,
      error: errError,
      stack,
      body = {}
    } = error
    const _msg = msg || message

    super(_msg)

    const { status, error: err } = body

    this._isESError = true
    this.name = name || ERROR_NAME
    this.classification = ERROR_CLASSIFICATION

    this.message = _msg
    this.msg = _msg

    this.statusCode = statusCode || status || 500

    this.error = err || (_isESError && errError) || (!_isESError && error) || undefined
    const thisErrorHasKeys = !!Object.keys(this.error || {}).length
    if (!thisErrorHasKeys) { this.error = undefined }

    this.stack = stack || (
      (CAN_CAPTURE && Error.captureStackTrace(this, ESError)) ||
      (CAN_STACK && new Error().stack) ||
      undefined
    )

    autoBind(this)
  }

  getResponseBody () {
    const { statusCode, message } = this
    const error = this.toJSON()

    const { NODE_ENV } = process.env
    error.stack = (NODE_ENV === 'production' && undefined) || error.stack

    return new ResponseBody(statusCode, message, undefined, error)
  }

  toJSON () {
    const { toJSON, ...rest } = this
    return JSON.parse(JSON.stringify(rest))
  }
}
