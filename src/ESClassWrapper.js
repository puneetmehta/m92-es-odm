'use strict'

export default function ESClassWrapper (Class, wrapperProps) {
  const { INDEX, TYPE, SETTINGS, PROPERTIES } = wrapperProps

  const indexFunc = () => INDEX
  const typeFunc = () => TYPE
  const settingsFunc = () => ({ ...SETTINGS })
  const propsFunc = () => ({ ...PROPERTIES })

  Class.__defineGetter__('_index', indexFunc)
  Class.__defineGetter__('_type', typeFunc)
  Class.__defineGetter__('_settings', settingsFunc)
  Class.__defineGetter__('_properties', propsFunc)
}
