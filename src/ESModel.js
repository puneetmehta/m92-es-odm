'use strict'

import _ from 'lodash'
import ESClient from './ESClient'
import ESError from './ESError'

const DEFAULT_SETTING = {
  index: {
    number_of_shards: 5,
    number_of_replicas: 1
  }
}

export default class ESModel {
  constructor (CONFIG, Class) {
    const { _index = '', _type = '', _settings = {}, _properties = {} } = Class
    this.CONFIG = CONFIG
    this.Class = Class
    this.index = _index
    this.type = _type
    this.settings = _settings
    this.properties = _properties

    // Method Hard-binding
    this.createIndex = this.createIndex.bind(this)
    this.removeIndex = this.removeIndex.bind(this)
    this.create = this.create.bind(this)
    this.createWithIndex = this.createWithIndex.bind(this)
    this.bulkCreate = this.bulkCreate.bind(this)
    this.recreate = this.recreate.bind(this)
    this.findById = this.findById.bind(this)
    this.list = this.list.bind(this)
    this.search = this.search.bind(this)
    this.update = this.update.bind(this)
    this.bulkUpdate = this.bulkUpdate.bind(this)
    this.remove = this.remove.bind(this)
    this.bulkRemove = this.bulkRemove.bind(this)
    this.removeBy = this.removeBy.bind(this)
  }

  async createIndex (_index) {
    const { CONFIG, index, settings, properties } = this
    const Client = new ESClient(CONFIG)
    const body = {
      settings: Object.assign({}, DEFAULT_SETTING, settings),
      mappings: {
        properties
      }
    }

    const params = {
      index: _index || index,
      body
    }

    try {
      const esResponse = await Client.indices.create(params)
      Client.close()
      return esResponse
    } catch (error) {
      Client.close()
      throw new ESError(error)
    }
  }

  async removeIndex (_index) {
    const { CONFIG, index, settings, properties } = this
    const Client = new ESClient(CONFIG)
    const body = {
      settings: Object.assign({}, DEFAULT_SETTING, settings),
      mappings: {
        properties: properties
      }
    }

    const params = {
      index: _index || index,
      body
    }

    try {
      const esResponse = await Client.indices.delete(params)
      Client.close()
      return esResponse
    } catch (error) {
      Client.close()
      throw new ESError(error)
    }
  }

  async create (attrs, dynamicIndex) {
    const { CONFIG, Class, index, type } = this
    const Client = new ESClient(CONFIG)
    const body = new Class(attrs)
    const { id } = body

    const params = {
      refresh: true,
      index: dynamicIndex || index,
      type,
      id,
      body
    }

    try {
      await Client.create(params)
      Client.close()
      return body
    } catch (error) {
      Client.close()
      throw new ESError(error)
    }
  }

  async recreate (attrs) {
    try {
      const indexExist = await this._indexExist()
      if (indexExist) {
        await this.removeIndex()
      }
      await this.createIndex()
      const createResponse = await this.bulkCreate(attrs)
      return createResponse
    } catch (error) {
      throw new ESError(error)
    }
  }

  async createWithIndex (attrs, options) {
    const { Class } = this
    const body = new Class(attrs)

    try {
      const index = this._getDynamicIndex(body, options)
      await this.createIndex(index)
      const createResponse = await this.create(attrs, index)
      return createResponse
    } catch (error) {
      throw new ESError()
    }
  }

  async bulkCreate (attrs) {
    const { CONFIG, Class, index: _index, type: _type } = this
    const body = []
    _.each(attrs, (doc) => {
      const { id: _id } = doc
      const actionObj = {
        create: { _index, _id, _type }
      }
      body.push(actionObj)
      const dataObj = new Class(doc)
      body.push(dataObj)
    })

    const params = {
      refresh: true,
      body
    }

    const Client = new ESClient(CONFIG)
    try {
      const bulkCreateResponse = await Client.bulk(params)
      const { items } = bulkCreateResponse
      const responseBody = {
        errorPresent: false,
        errors: [],
        items: []
      }
      _.each(items, (responseRecord, index) => {
        const operation = Object.keys(responseRecord)[0]
        const item = responseRecord[operation]
        const bodyItem = body[(((index + 1) * 2) - 1)]
        if (item.status === 201) {
          responseBody.items.push(new Class({
            ...bodyItem,
            id: item._id
          })
          )
        } else {
          responseBody.errorPresent = true
          responseBody.errors.push({
            item: new Class(bodyItem),
            message: item.error.reason
          })
        }
      })
      Client.close()
      return responseBody
    } catch (error) {
      Client.close()
      throw new ESError(error)
    }
  }

  async findById (id) {
    const { CONFIG, Class, index, type } = this
    const Client = new ESClient(CONFIG)

    const params = {
      refresh: true,
      index,
      type,
      id
    }

    try {
      const { _source } = await Client.get(params)
      Client.close()
      const instance = new Class(_source)
      return instance
    } catch (error) {
      Client.close()
      throw new ESError(error)
    }
  }

  async list () {
    const { CONFIG, Class, index, type } = this
    const Client = new ESClient(CONFIG)
    const body = {
      query: {
        match_all: {}
      }
    }

    const params = {
      index,
      type,
      body
    }

    try {
      const esRecord = await Client.search(params)
      let { hits: { hits } } = esRecord
      hits = _.chain(hits).sortBy('_score').map((hit) => {
        const { _score, _source } = hit
        const instance = new Class(_source)
        instance._score = _score
        return instance
      }).value()
      Client.close()
      return hits
    } catch (error) {
      Client.close()
      throw new ESError(error)
    }
  }

  async search (body) {
    const { CONFIG, Class, index, type } = this
    const Client = new ESClient(CONFIG)

    const params = {
      index,
      type,
      body
    }

    try {
      const esRecord = await Client.search(params)
      let { hits: { hits } } = esRecord
      hits = _.chain(hits).sortBy('_score').map((hit) => {
        const { _score, _source } = hit
        const instance = new Class(_source)
        instance._score = _score
        return instance
      }).value()
      Client.close()
      return hits
    } catch (error) {
      Client.close()
      throw new ESError(error)
    }
  }

  async update (attrs) {
    const { CONFIG, index, type, Class } = this
    const Client = new ESClient(CONFIG)
    const doc = new Class(attrs)
    const { id } = doc
    const body = { doc }

    const params = {
      refresh: true,
      index,
      type,
      id,
      body
    }

    try {
      await Client.update(params)
      Client.close()
      return doc
    } catch (error) {
      Client.close()
      throw new ESError(error)
    }
  }

  async bulkUpdate (attrs) {
    const { CONFIG, Class, index: _index, type: _type } = this

    const body = []
    _.each(attrs, (doc) => {
      const { id: _id } = doc
      const actionObj = {
        update: { _index, _id, _type }
      }
      body.push(actionObj)
      const dataObj = { doc: new Class(doc) }
      body.push(dataObj)
    })

    const params = {
      refresh: true,
      body
    }

    const Client = new ESClient(CONFIG)
    try {
      const bulkCreateResponse = await Client.bulk(params)
      const { items } = bulkCreateResponse
      const responseBody = {
        errorPresent: false,
        errors: [],
        items: []
      }
      _.each(items, (responseRecord, index) => {
        const operation = Object.keys(responseRecord)[0]
        const item = responseRecord[operation]
        const bodyItem = body[(((index + 1) * 2) - 1)].doc
        if (item.status === 200) {
          responseBody.items.push(
            new Class({
              ...bodyItem,
              id: item._id
            })
          )
        } else {
          responseBody.errorPresent = true
          responseBody.errors.push({
            item: new Class(bodyItem),
            message: item.error.reason
          })
        }
      })
      Client.close()
      return responseBody
    } catch (error) {
      Client.close()
      throw new ESError(error)
    }
  }

  async remove (id) {
    const { CONFIG, index, type } = this
    const Client = new ESClient(CONFIG)

    const params = {
      refresh: true,
      index,
      type,
      id
    }

    try {
      await Client.delete(params)
      Client.close()
      return { status: 'Success' }
    } catch (error) {
      Client.close()
      throw new ESError(error)
    }
  }

  async bulkRemove (ids) {
    const { CONFIG, index: _index, type: _type } = this
    const body = []
    _.each(ids, ({ id: _id }) => {
      const actionObj = {
        delete: { _index, _type, _id }
      }
      body.push(actionObj)
    })

    const params = {
      refresh: true,
      body
    }

    const Client = new ESClient(CONFIG)
    try {
      const bulkDeleteResponse = await Client.bulk(params)
      const { items } = bulkDeleteResponse
      const responseBody = {
        errorPresent: false,
        errors: [],
        items: []
      }

      _.each(items, (responseRecord, index) => {
        const operation = Object.keys(responseRecord)[0]
        const item = responseRecord[operation]
        if (item.status === 200) {
          responseBody.items.push(item._id)
        } else {
          responseBody.errorPresent = true
          responseBody.errors.push({
            id: item._id,
            message: item.result
          })
        }
      })
      Client.close()
      return responseBody
    } catch (error) {
      Client.close()
      throw new ESError(error)
    }
  }

  async removeBy (query) {
    const { CONFIG, index } = this
    const Client = new ESClient(CONFIG)
    const body = { query }

    const params = {
      refresh: true,
      index,
      body
    }

    try {
      const esResponse = await Client.deleteByQuery(params)
      Client.close()
      return esResponse
    } catch (error) {
      Client.close()
      throw new ESError(error)
    }
  }

  async _indexExist (_index) {
    const { CONFIG, index, settings, properties } = this
    const Client = new ESClient(CONFIG)
    const body = {
      settings: Object.assign({}, DEFAULT_SETTING, settings),
      mappings: {
        properties: properties
      }
    }

    const params = {
      index: _index || index,
      body
    }

    try {
      const esResponse = await Client.indices.exists(params)
      Client.close()
      return esResponse
    } catch (error) {
      Client.close()
      throw new ESError(error)
    }
  }

  _getDynamicIndex (body, options) {
    const { getDynamicIndex } = options
    if (typeof getDynamicIndex !== 'function') {
      const err = new ESError(new Error("Require 'getDynamicIndex()' to Create Document"))
      throw err
    }

    const index = getDynamicIndex(body)
    if (!index) {
      const err = new ESError(500, "Invalid Index Found: '" + index + "'")
      throw err
    } else {
      return index
    }
  }
}
