'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _elasticsearch = require("elasticsearch");

class ESClient {
  constructor(CONFIG) {
    var {
      ACCESS_KEY_ID = '',
      SECRET_ACCESS_KEY = '',
      REGION = '',
      HOST = ''
    } = CONFIG;
    var ES_CONFIG = {
      service: 'es',
      region: REGION,
      host: HOST,
      accessKeyId: ACCESS_KEY_ID,
      secretAccessKey: SECRET_ACCESS_KEY
    };
    return new _elasticsearch.Client(ES_CONFIG);
  }

}

exports.default = ESClient;