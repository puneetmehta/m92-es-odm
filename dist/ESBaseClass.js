'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _v = _interopRequireDefault(require("uuid/v4"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class ESBaseClass {
  constructor() {
    var body = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    var _this = this;

    var {
      PROPERTIES = {}
    } = options;
    Object.keys(PROPERTIES).forEach(prop => {
      var config = PROPERTIES[prop];
      var {
        type
      } = config;
      var value = body[prop] || '';

      if (type === 'integer' || type === 'long') {
        value = _lodash.default.isArray(value) ? value : Number(value);
      }

      if (type === 'boolean') {
        value = value === true || _lodash.default.chain(value).toString().lowerCase().trim().value() === 'true';
      }

      if (type === 'text' || type === 'keyword') {
        value = _lodash.default.isArray(value) ? value : _lodash.default.chain(value).toString().value();
      }

      if (type === 'object') {
        value = _lodash.default.isObject(value) ? value : {};
      }

      _this[prop] = value;
    });

    if (options.timestamp === true) {
      this.createdAt = body.createdAt || new Date().getTime();
      this.updatedAt = body.updatedAt || this.createdAt;
    }

    this.id = this.id || options.generateId && (0, _v.default)() || '';
  }

}

exports.default = ESBaseClass;